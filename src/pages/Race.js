import React  from 'react';
import TypeRace from '../components/TypeRace/TypeRace';

const race = () => {
        return(
            <div>
                <TypeRace quote = "Looking back, I saw that for my whole conscious life I had not understood either myself or my strivings. What had seemed for so long to be beneficial now turned out in actuality to be fatal, and I had been striving to go in the opposite direction to that which was truly necessary to me." />
            </div>
        );
    }

export default race;